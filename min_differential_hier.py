import numpy as np
from scipy.optimize import minimize
import time

string = "test"

string = [*string]
global value 
value = [[ord(d) for d in string], [ord(d) for d in string]]
print(value, 'test')
global s
def dif(x0):
    global s
    s = np.random.normal(loc=x0[0], scale=x0[1], size=4)
    loss = value-s
    gradient = np.sum(np.gradient(loss))
    print(s, 't')
    return(gradient)


global x0
x0 = [np.mean(value[0]), np.std(value[1])]
#print(dif(x0), 'test')
res = minimize(dif, x0, method='Nelder-Mead', tol=1e-120)

